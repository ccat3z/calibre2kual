#!/usr/bin/python

import os
import json
import sys
import shutil
import hashlib

calibredb_cmd = "calibredb.exe list --for-machine -f formats,id,title,tags,uuid --sort-by=title --ascending"
preference_formats = ['azw3', 'pdf', 'mobi', 'txt', 'doc']

class Book:
    def __init__(self, title, id, uuid, tags, formats):
        self.title = title
        self.id = id
        self.uuid = uuid
        self.tags = tags
        self.formats = formats

    def getFileFotmat(self, format):
        for f in self.formats:
            if f.split('.')[-1] == format:
                return f.replace("E:", "/mnt/e")

    def getFileFotmats(self, preference_formats):
        for format in preference_formats:
            f=self.getFileFotmat(format)
            if f is not None:
                return f

class Library:
    def __init__(self):
        self.__books = []

    def getBooks(self):
        return self.__books

    def addBook(self, book):
        if isinstance(book, Book):
            self.__books.append(book)

    def len(self):
        return len(self.__books)

    def getBooksByTags(self, tag):
        books = []
        for book in self.__books:
            for book_tag in book.tags:
                if book_tag.find(tag) == 0:
                    books.append(book)
                    break

        return books

def filenamize(str):
    special_characters = {
    '/': '',
    ':': '_',
    '?': '_'
    }

    for (k, v) in special_characters.items():
        str = str.replace(k, v)

    return str

books = json.loads(os.popen(calibredb_cmd).read());

library = Library()

for book_json in books:
    book = Book(book_json['title'], book_json['id'], book_json['uuid'], book_json['tags'], book_json['formats'])
    library.addBook(book)

json_menu = {"items": [{"name": "library", "items":[]}]}

for book in library.getBooks():
    src_file = book.getFileFotmats(preference_formats)
    tag = book.tags[0].replace(".", "/")
    id = book.id
    if "*" in book.tags:
        print("Skip " + book.title + " due to the * in tags")
    elif src_file is not None and "*" not in book.tags:
        file_name = filenamize(book.title) + "." + filenamize(src_file.split('.')[-1])
        target_file = tag + "/" + file_name

        tags = tag.split("/")
        root = json_menu["items"][0]["items"]
        for tag_menu in tags:
            has = False
            for item in root:
                if item["name"] == tag_menu:
                    has = True
                    root = item["items"]
                    break
            if not has:
                root.append({"name": tag_menu, "items":[]})
                root = root[-1]["items"]

        root.append({
        "name": book.title,
        "action": "cp " + "\"/mnt/us/library/"  + target_file + "\" " + "\"/mnt/us/documents/" + file_name + "\"",
        "exitmenu": False,
        "refresh": False,
        "status": False,
        "internal": "status Copy " + file_name
        })

        if not os.path.isdir(tag):
            os.makedirs(tag)
        print("\033[34mcp\033[0m " + src_file + " \033[34m->\033[0m " + target_file)
        shutil.copy2(src_file, target_file)
    else:
        print("Not found satisfied format of " + book.title)


with open('menu.json', 'wt') as f:
    print(json.dumps(json_menu, ensure_ascii=False), file=f)
