#!/bin/sh

if [ -d library ];then
    echo "library dir existed."
    read -n 1 -p "remove it?[y/N]" answer
    echo
    if [ "$answer" == "y" ];then
        rm -rf library

        mkdir library
        cd library
        ../calibre2kual.py
        cd ..
    fi
else
    mkdir library
    cd library
    ../calibre2kual.py
    cd ..
fi

read -n 1 -p "rsync?[y/N]" answer
echo
if [ "$answer" == "y" ];then
    rsync -a -v --delete -W library /mnt/d/
    rsync -a -v --delete library/menu.json /mnt/d/extensions/lib/ 
else
    exit
fi
